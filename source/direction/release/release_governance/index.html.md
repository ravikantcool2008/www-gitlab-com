---
layout: markdown_page
title: "Category Direction - Release Governance"
---

- TOC
{:toc}

## Release Governance

Release Governance is about addressing the demand of the business to understand what is changing in your software. Our focus is on supporting the variety of controls and automation (security, compliance, or otherwise) to ensure your releases are managed in an auditable and trackable way. 

The backbone of this category is a strong integration with CI/CD to create an auditable chain of custody for artifacts, commits, issues within a release, as well as if the release has satisfied quality and security gates. Table stakes for enterprise-grade governance includes traceability of automated actions alongside the gathering of appropriate approvals throughout the release process. Our intention is to streamline the experience of preparing for an audit or compliance review as an organic byproduct of using GitLab.

Release Governance is complemented by the tangential category within Release of [Secrets Management](/direction/release/secrets_management). Also related is [Requirements Management](/direction/plan/requirements_management) from the [Plan](/direction/plan/) stage.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Governance)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1297) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Now that we have delivered the first snapshot of Release Evidence, ([gitlab#26019](https://gitlab.com/gitlab-org/gitlab/issues/26019)), we are expanding the snapshot to be at time of release deployment in https://gitlab.com/gitlab-org/gitlab/issues/38103. By making both of these snapshots available in the release itself, we help tell the story of  what, where, when, and by whom any change in the software happened, without any additional effort. Up next, includes supporting users throughout the audit and compliance review process with ease it a priority for GitLab, which includes tracking the release events within GitLab audit logs (https://gitlab.com/gitlab-org/gitlab/issues/32807).  

A consistent challenge across GitLab users is being able to track deployments to GitLab.com, which is a top focus for [gitlab-infra#477](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/477). 
 
We have learned there is demand to support the logging auto-stop actions ([gitlab#36407](https://gitlab.com/gitlab-org/gitlab/issues/36047)). We are continuing to investigate the needs around access control by users and roles for protected enviornments in[gitlab#36665](https://gitlab.com/gitlab-org/gitlab/issues/36665)).


## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:

- [Wait for approvals in pipelines](https://gitlab.com/gitlab-org/gitlab/issues/15819) (Complete)
- [Evidence collection for Releases - snapshot of release metadata at moment of release](https://gitlab.com/gitlab-org/gitlab/issues/26019) (Complete)
- [Capture Release actions in the audit log page](https://gitlab.com/gitlab-org/gitlab/issues/32807) (12.7)

Following the delivery of the above, we would be working toward next maturity target of Viable:

- [Evidence collection for Releases - snapshot of release metadata at moment of deployment](https://gitlab.com/gitlab-org/gitlab/issues/38103) (12.8)
- [User access policy for Production environment](https://gitlab.com/gitlab-org/gitlab/issues/15778)
- [Blackout periods for environments MVC](https://gitlab.com/gitlab-org/gitlab/issues/24295)
- [Binary Authorization MVC](https://gitlab.com/gitlab-org/gitlab/issues/7268)

## Competitive Landscape

A key capability of products which securely manage releases is to collect
evidence associated with releases in a secure way. In [gitlab#26019](https://gitlab.com/gitlab-org/gitlab/issues/26019)
we introduced a new kind of entity that is part of a release, which contains
various kinds of evidence. As we expand the evidence to include test results, security scans, and other artifacts that
can be collected as part of a release generation, is a great differentiator for GitLab. This will uniquely enable us to be a single application for the DevOps lifecycle.

## Analyst Landscape

The analysts in this space tend to focus a lot right now on existing, more
legacy style deployment workflows so changes like [gitlab#9187](https://gitlab.com/gitlab-org/gitlab/issues/9187)
(better support for validation of approvals in the pipeline) will help
us perform better here, as well as for the kinds of customers who are
really need a bit more control over their delivery process.

Similarly, integrations with technologies like ServiceNow ([gitlab#8373](https://gitlab.com/gitlab-org/gitlab/issues/8373))
will help GitLab fit in better with larger, pre-existing enterprise governance workflows.

## Top Customer Success/Sales Issue(s)

The CS team frequently sees requests for integration with ServiceNow to support change
management in CD pipelines, as per [gitlab#8373](https://gitlab.com/gitlab-org/gitlab/issues/8373).

## Top Customer Issue(s)

Enabling the upload of assets to releases (https://gitlab.com/gitlab-org/gitlab/issues/17838), within release evidence satifies many customers' needs to show the entirety of release and further strengthens the releae page as a single source of truth.  

Another issue referenced by a handful of customers interested in enterprise-grade governance, includes enforcing the signtaure validation of containers during deployment as captured in our [Binary Authorization MVC](https://gitlab.com/gitlab-org/gitlab/issues/7268. 

## Top Internal Customer Issue(s)

Implementing user access controles for environments in [gitlab#15778](https://gitlab.com/gitlab-org/gitlab/issues/15778), which
would satify the internal delivery team request to allow for more secure, locked down access to production-type
environments instead of relying on more broad-based project permissions.

### Features of Interest

 - [Releases Page](https://gitlab.com/gitlab-org/gitlab-ce/releases)
 - [gitlab#9187](https://gitlab.com/gitlab-org/gitlab/issues/9187) (Approval jobs)
 - [gitlab#26019](https://gitlab.com/gitlab-org/gitlab/issues/26019) (Evidence collection)
 - [gitlab#36665](https://gitlab.com/gitlab-org/gitlab/issues/36665) (Production Access Policies)

### Ongoing efforts to support

 - [Internal change management process](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/231)
 - [Service lifecycle workflow](/handbook/engineering/security/guidance/SLC.1.01_service_lifecycle_workflow.html)
 - [Controls by family](/handbook/engineering/security/sec-controls.html#list-of-controls-by-family)

## Top Vision Item(s)

Important for this category (though also expansive and includes a few others) is
our epic for [locking down the path to production](https://gitlab.com/groups/gitlab-org/-/epics/762),
which will help us successfully deliver compliance controls within the software delivery pipeline.

Also related to locking down the path to production is binary
authorization ([gitlab#7268](https://gitlab.com/gitlab-org/gitlab/issues/7268))
which provides a secure means to validate deployable containers. At the
moment however this only works with GKE so ultimate user adoption is limited. As such we're
keeping an eye on adoption, but have not yet implemented an MVC.

Blackout periods ([gitlab#24295](https://gitlab.com/gitlab-org/gitlab/issues/24295))
will help compliance teams enforce periods where production needs to remain stable/not change. We are beginning to research blackout period relevancy and if you are interested in participating in research tune into [gitlab#146](https://gitlab.com/gitlab-org/ux-research/issues/146).
