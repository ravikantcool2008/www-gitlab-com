---
layout: markdown_page
title: "Category Direction - Jupyter Notebooks"
---

- TOC
{:toc}

## Jupyter Notebooks

Jupyter Notebooks are a common type of code used for data-science use cases. With GitLab you can store and version control those notebooks in the same way you store packages and application code.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=jupyter)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)
- [Documentation](https://docs.gitlab.com/ee/user/project/repository/#jupyter-notebook-files)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Use cases

1. Explore and visualize data.
1. Work collaboratively and securly by checking your notebook into version control.
1. Create and share environments for hosting notebooks.
1. Share notebooks and code snippets with team members.

## What's next & why

This is a new category that is in the [minimal stage](https://about.gitlab.com/direction/maturity/index.html). 

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:

- TBD

## Competitive landscape

* [Domino Data Lab](https://www.dominodatalab.com/)
* [Dataiku](https://www.dataiku.com/)
* [Rapid Miner](https://rapidminer.com/) 

​​
## Top Customer Success/Sales issue(s)
TBD

## Top user issue(s)
TBD

## Top internal customer issue(s)
TBD

## Top Vision Item(s)
TBD
