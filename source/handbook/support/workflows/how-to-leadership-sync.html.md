---
layout: handbook-page-toc
title: How to organize a weekly leadership sync
category: General
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Overview

As a larger leadership team, it's important that we remain in constant communication to ensure that we're
building relationships and avoiding siloing.

The purpose of these meetings is covered in the [Leadership Meetings](/handbook/support/index.html#leadership-meetings) section
of the Support Handbook.

It is worth reiterating though that these meetings are not for making decisions or discussing things that
could be discussed within issues.

---
#### Understandings
1. Most agenda items should be linked to an existing issue. If it does not have an existing issue, raise one before putting it in the agenda.
1. Agenda items that do not need to be linked to an existing issue are:
   - Discussion surrounding a specific individual's promotion, performance or individual situation.
1. At the beginning of a week, a specified manager will create the agenda issue in the [leadership-sync project](https://gitlab.com/gitlab-com/support/managers/leadership-sync) using the [agenda template](https://gitlab.com/gitlab-com/support/managers/leadership-sync/issues/new?issuable_template=Agenda) and assign it to `@gitlab-com/support/managers`
1. All discussion items should be put in the agenda by the end of your working day on Tuesday.
1. Each meeting should have a chairperson to ensure that voices are heard equally and we make progress through the agenda.
1. All participants should be familiar with the [Video Calls Section](/handbook/communication/#video-calls) of the Communication page in the Handbook.

#### Procedure
##### As a chair
1. Review the meeting agenda and familiarize yourself with each item. 
1. Review the hiring data and be aware of anything that is of note for the regions that are meeting.
1. Review the upcoming support events. Be sure to mention anything that may have an action item, or may need an action item such as:
   1. *Group Conversation*: _Please take a look at the slides, and add any points of interest for the general company_
   1. *Holiday*: _Do we have (or need) a coverage plan for this holiday?_
   1. *Metrics Review*: _Is there anything that needs to be highlighted to the executive team?_ 
1. Be aware of when people unmute - this is an indication that they have something to say. If needs be, please interrupt and pass the
floor. We want everyone to contribute, so it's your job to make sure that this can happen.
1. Assign any action items - including summarizing the discussion in an issue.

##### As a manager
* Before the meeting: 
    * add agenda items before Tuesday at the end of your working day
    * review each agenda item. If you have any feedback, don't hold it for the meeting but comment directly on the issue.
* During the meeting:
    * be brief: everyone should be familiar with the discussion
    * be clear: why did you bring this item to this meeting?
    * be polite: avoid interrupting when possible (For more on this see point 13 in the [Video Calls Section](/handbook/communication/#video-calls) of the Communication page)
* After the meeting: 
   * note any points that you made during the meeting that were relevant to the discussion.

