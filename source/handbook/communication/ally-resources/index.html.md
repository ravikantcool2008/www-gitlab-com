---
layout: handbook-page-toc
title: "Ally resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is an ally? 

A diversity and inclusion "ally" is someone who is willing to take action in support of another person, in order to remove barriers that impede that person from contributing their skills and talents in the workplace or community.

## How to be an ally 

It is not required to be an ally to work at GitLab. At GitLab it is required to [be inclusive](/handbook/values/#diversity--inclusion). Being an ally goes a step beyond being inclusive to taking action to support marginalized groups. The first step in being an ally is self-educating. This is a page to list resources that GitLab team-members have found helpful in learning how to act as an ally.

## What it means to be an ally
* Take on the struggle as your own
* Stand up, even when you feel uncomfortable
* Transfer the benefits of your privilege to those who lack it
* Acknowledge that while you, too, feel pain, the conversation is not about you

## Concepts & Terms

1. Privilege: an unearned advantage given to some people but not all 
1. Oppression: systemic inequality present throughout society that benefits people with more privilege and is a disadvantage to those with fewer privileges 
1. Ally: a member of a social group that has some privilege, that is working to end oppression and understands their own privilege 
1. Power: The ability to control circumstances or access to resources and/or privileges  
1. Marginalized groups:  a person or group that are treated as unimportant, insignificant or of lower status. In a workplace setting, employees could be treated as invisible, as if they aren't there or their skills or talents are unwelcome or unnecessary

## Tips on being an ally

1. Identifying your power and privilege helps you act as an ally more effectively
1. Follow and support those as much as possible from marginalized groups 
1. When you make a mistake, apologize, correct yourself, and move on
1. Allies spend time educating themselves and use the knowledge to help
1. Allies take the time to think and analyze the situation
1. Allies try to understand Perception vs. Reality
1. Allies don’t stop with their power they also leverage others powers of authority

## GitLab diversity and inclusion resources 

Allies familiarize themselves with GitLab's general D&I content

- [Diversity and Inclusion page](/company/culture/inclusion/)
- [D&I training](/company/culture/inclusion/#employee-training-and-learning-opportunities)
- [Unconscious bias](/handbook/communication/unconscious-bias/)

## Resources

Here are additional resources on being an ally

- [Guide to allyship](https://www.guidetoallyship.com)
- [5 Tips For Being An Ally](https://www.youtube.com/watch?v=_dg86g-QlM0)
- [Ally skills workshop](https://frameshiftconsulting.com/ally-skills-workshop/). Check out the materials section with [a handout PDF](https://files.frameshiftconsulting.com/Ally%20Skills%20Workshop%20handout%20-%20Letter.pdf) (linking to many more resources), [slides PDF](https://files.frameshiftconsulting.com/Ally%20Skills%20Workshop%20slides.pdf), [videos](https://www.youtube.com/watch?v=wob68Nl2440), and more.
- [Why cisgender allies should put pronouns on their name tag](https://medium.com/@mrsexsmith/dear-cis-people-who-put-your-pronouns-on-your-hello-my-name-is-nametags-78c047ed7af1)

## Ally Training 

We have a 50 minute [Live Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning) Ally Training scheduled for 2020-01-28. Once that training has been completed, the recording will be uploaded to this page for viewing. 
